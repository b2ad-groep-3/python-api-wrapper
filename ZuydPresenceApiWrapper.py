import json
import time
import hmac
import hashlib
import requests
import shutil
import os

class ZuydPresenceApiWrapper():

    def __init__(self, config_path: str = "config.json") -> None:
        """Initializes the class and loads the config.

        Args:
            config_path (str, optional): path to a config file. Defaults to "config.json".
        """        

        self.config = self.load_config(config_path)
        self.hmac_algo = hashlib.sha256
        self.hmac_glue = ":"
        self.url_glue = "/"

    def update_image_database(self, output_path: str) -> None:
        """Pulls the image database from the backend.

        Args:
            output_path (str): path to extract the images to. Must end with '/'.
        """        

        authorization = self.header("GET")

        url = self.url_glue.join([self.config["url"], self.config["version"], self.config["endpoints"]["image_database"]])

        local_file = output_path + 'export.zip'

        with requests.get(url, stream=True, headers={
            self.config["credentials"]["header"]: authorization,
            "Content-Type": "application/json"
        }) as response:
            with open(local_file, 'wb') as localfile:
                shutil.copyfileobj(response.raw, localfile)

        shutil.unpack_archive(local_file, output_path)
        os.remove(local_file)

    def teacher(self, teacher_id: int):
        """Gets a single teacher by their ID.

        Args:
            teacher_id (int): the id of the teacher to get.

        Returns:
            requests.Response: response of the request.
        """        

        authorization = self.header("GET")

        url = self.url_glue.join([self.config["url"], self.config["version"], self.config["endpoints"]["teacher_by_id"], str(teacher_id)])

        response = requests.get(url, headers={
            self.config["credentials"]["header"]: authorization,
            "Content-Type": "application/json"
        })

        return response

    def teachers(self):
        """Gets a JSON object with all teachers.

        Returns:
            requests.Response: response of the request.
        """        

        authorization = self.header("GET")

        url = self.url_glue.join([self.config["url"], self.config["version"], self.config["endpoints"]["teachers"]])

        response = requests.get(url, headers={
            self.config["credentials"]["header"]: authorization,
            "Content-Type": "application/json"
        })

        return response
    
    def set_status(self, image_name: str, status: int, branch_id: int = None):
        """Updates a status for a teacher using a photo that belongs to them.

        Args:
            image_name (str): file name of detected image with file extension.
            status (int): 0 or 1 based on what the target status should be.
            branch_id (int): id of the branch that the teacher was detected at. Defaults to the branch_id in the config file.

        Returns:
            requests.Response: response of the request.
        """        

        if(branch_id == None):
            branch_id = self.config["branch_id"]

        body = "{" + f'"image": "{image_name}", "branch_id": {branch_id}, "present": {status}' + "}"

        url = self.url_glue.join([self.config["url"], self.config["version"], self.config["endpoints"]["status_by_image"]])

        authorization = self.header("POST", body)

        print(body)

        response = requests.post(url, data=body, headers={
            self.config["credentials"]["header"]: authorization,
            "Content-Type": "application/json"
        })

        return response

    def verify_hmac_functionality(self):
        """Verifies the HMAC-authentication of the API.

        Returns:
            response: response object.
        """        

        authorization = self.header("GET")

        url = self.url_glue.join([self.config["url"], self.config["version"], self.config["endpoints"]["verification"]])

        response = requests.get(url, headers={
            self.config["credentials"]["header"]: authorization,
            "Content-Type": "application/json"
        })

        return response
    
    def verify_hmac_functionality_with_body(self):
        """Verifies the HMAC-authentication of the API when sending a request with body.

        Returns:
            response: response object.
        """

        body = '{"name": "Janssen"}'

        authorization = self.header("GET", body)

        url = self.url_glue.join([self.config["url"], self.config["version"], self.config["endpoints"]["verification"]])

        response = requests.get(url, data=body, headers={
            self.config["credentials"]["header"]: authorization,
            "Content-Type": "application/json"
        })

        return response    

    def header(self, http_scheme: str, body = None) -> str:
        """Generates a ZuydPresence compatible authorization header.

        Args:
            http_scheme (str): GET/POST/PUT/DELETE
            body (json string, optional): request body in json. Defaults to None.

        Returns:
            str: header in string format.
        """        

        epoch = self.epoch_timestamp()

        http_scheme = http_scheme.upper()

        signature = self.signature(http_scheme, epoch, body)
        header = self.hmac_glue.join([http_scheme, self.config["credentials"]["key"], signature, epoch])

        return header

    def signature(self, http_scheme: str, epoch: str, body = None) -> str:
        """Generates a ZuydPresence compatible HMAC-signature.

        Args:
            http_scheme (str): GET/POST/PUT/DELETE
            epoch (str): epoch timestamp.
            body (json string, optional): request body in json. Defaults to None.

        Returns:
            str: signature string.
        """        

        if(body != None):
            body = hashlib.md5(body.encode()).hexdigest()
            hmac_message = self.hmac_glue.join([http_scheme, self.config["credentials"]["key"], body, epoch])
            hmac_message = hmac_message.encode()
        else:
            hmac_message = self.hmac_glue.join([http_scheme, self.config["credentials"]["key"], epoch])
            hmac_message = hmac_message.encode()

        signature = hmac.new(bytes(self.config["credentials"]["secret"], "UTF-8"), hmac_message, self.hmac_algo).hexdigest()

        return signature

    
    def load_config(self, path: str = "config.json") -> dict:
        """Reads a json config file into a dictionary.

        Args:
            path (str, optional): path to config json. Defaults to "config.json".

        Returns:
            dict: config file object.
        """        

        with open(path) as config:
            self.config = json.load(config)
        
        return self.config

    @staticmethod
    def epoch_timestamp():
        """Returns an epoch timestamp (to overcome timezone mismatches).

        Returns:
            int: seconds (rounded) from epoch time.
        """        

        return str(int(time.time()))