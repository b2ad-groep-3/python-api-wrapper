from ZuydPresenceApiWrapper import ZuydPresenceApiWrapper

# /**
#  * This example allows you to verify whether the HMAC-authentication is working correctly.
#  *
#  * If properly configured, you will receive a successful status code (200).
#  * If not properly configured, you will receive an unauthorized status code (401).
#  */

api = ZuydPresenceApiWrapper()

print(api.verify_hmac_functionality().content)
print(api.verify_hmac_functionality_with_body().content)