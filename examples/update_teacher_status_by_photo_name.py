from ZuydPresenceApiWrapper import ZuydPresenceApiWrapper

# /**
#  * This example allows you to update a teacher's status by sending
#  * a request with the name of a photo that belongs to them.
#  *
#  * The target branch can be set in the request or default to the
#  * one specified within the configuration file.
#  *
#  * The ID's of the branches can be found in the management app.
#  */

api = ZuydPresenceApiWrapper()

print(api.set_status("b8124748b8637ad72279be686b816d1c.jpg", 1, 1).content)