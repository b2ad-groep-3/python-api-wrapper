from ZuydPresenceApiWrapper import ZuydPresenceApiWrapper

# /**
#  * This example shows how to retrieve a single teacher by ID.
#  */

api = ZuydPresenceApiWrapper()

print(api.teacher(1).content)