from ZuydPresenceApiWrapper import ZuydPresenceApiWrapper

# /**
#  * This example shows how to retrieve all the teachers in the system.
#  */

api = ZuydPresenceApiWrapper()

print(api.teachers().content)